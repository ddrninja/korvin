# korvin (alpha)

Korvin is a virtual assistant designed to live in your terminal. Korvin does not spy on you and can provide you with usefull data from external services.

## Installation

```
npm install -g korvin
```

## Usage

```
korvin [service] [param]
```

```
Available Services:
 help       Displays this list.
 cs         Returns cheat sheet for given topic. (cheat.sh)
 wttr       Returns weather report. (wttr.in)
 dict       Returns dictionary definition of given word. (dict.org)
 news       Returns news for given topic. (getnews.tech)
 crypto     Returns crypto stock report. (rate.sx)
 country    Returns country name based on your IP address. (ifconfig.co)
 city       Returns city name based on IP your addres. (ifconfig.co)
 ip         Returns your extermal IP address. (ifconfig.co)
```

## Examples

```
korvin wttr Chicago
```
Will show you weather report for Chicago.
```
korvin dict linux
```
Will return dictionary definition of word "linux".
```
korvin cs js/prototypes
```
Will return information (cheat sheet) about prototypes in Java Script.

## Configuration

Korvin will create config file for you if there is no such file in your system. Check out:
```
~/.config/korvin/korvinrc.json
```

Default config file looks like this:
```json
{
    "news": {
        "country": "us",
        "category": false,
        "number": 3,
        "reverse": false,
        "nocolor": false
    },
    "wttr": {
        "location": "New York",
        "units": "m",
        "view": "n"
    }
}
```
