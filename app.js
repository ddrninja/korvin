#!/usr/bin/env node

const { spawn } = require('child_process')
const http = require('http')
const chalk = require('chalk')
const fs = require('fs')
const os = require('os')
const log = console.log

const configDir = `${os.homedir()}/.config/korvin`
const configFile = 'korvinrc.json'
const configFilePath = `${configDir}/${configFile}`

const helpText = `Usage:
 korvin [service] [params]

Available Services:
 help       Displays this list.
 cs         Returns cheat sheet for given topic. (cheat.sh)
 wttr       Returns weather report. (wttr.in)
 dict       Returns dictionary definition of given word. (dict.org)
 news       Returns news for given topic. (getnews.tech)
 crypto     Returns crypto stock report. (rate.sx)
 country    Returns country name based on your IP address. (ifconfig.co)
 city       Returns city name based on IP your addres. (ifconfig.co)
 ip         Returns your extermal IP address. (ifconfig.co)`

let config = configuration()

function configuration() {

    let defaults = {
        "news": {
            "country": "us",
            "category": false,
            "number": 3,
            "reverse": false,
            "nocolor": false,
        },
        "wttr": {
            "location": "New York",
            "units": "m",
            "view": "n",
        }
    }

    // Creates config directory if there is no one yet.
    if (!fs.existsSync(configDir)) {
        fs.mkdirSync(configDir)
    }

    // If there are no config file defaults will be saved
    if (!fs.existsSync(configFilePath)) {
        fs.writeFileSync(`${configDir}/${configFile}`, JSON.stringify(defaults, null, 2))
    }

    // Returns config JSON from config file
    // TODO: config file has to overwrite defaults, not replace
    return JSON.parse(fs.readFileSync(configFilePath))
}

let params = process.argv
let envPath = params.shift()
let appPath = params.shift()
let action = params.shift()

if (typeof action === 'undefined') {

    help()

} else {

    switch (action) {

        case 'wttr':
            wttr(config.wttr, params)
            break
        case 'country':
            curlGet('ifconfig.co', '/country')
            break
        case 'city':
            curlGet('ifconfig.co', '/city')
            break
		case 'ip':
			curlGet('ifconfig.co', '/')
			break
        case 'dict':
            dictionary(params)
            break
        case 'news':
            news(config.news, params)
            break
        case 'crypto':
            curlGet('rate.sx', '/')
            break
        case 'cs':
            cheatSheet(params)
            break
        case 'help':
            help();
            break;
        default:
            console.log('Are you crazy?')
    }
}

function help() {

    console.log(helpText)
}

function wttr(cfg, params) {

    let location = ''

    if (params.length) {
        params.forEach((param) => {
            if (location.length)
                location += '+'
            location += param
        })
    }
    
    location = location ? location : cfg.location.replace(' ', '+')

    let path = `/${location}?lang=${cfg.lang}&${cfg.view}`
    curlGet('wttr.in', path)
}

function cheatSheet(params) {
    let path = '/'


    params.forEach((param) => {
        if (path !== '/') {
            path += '+'
        }

        path += param
    })
    
    curlGet('cheat.sh', path)
}

function news(cfg, params) {
    
    let path = '/'

    params.forEach((param) => {
        if (path !== '/')
            path += '+'
        path += param
    })
    
    if (cfg.category) {
        path += `,category=${cfg.category}`
    }

    if (cfg.number) {
        path += `,n=${cfg.number}`
    }

    if (cfg.reverse) {
        path += 'reverse=true'
    }

    if (cfg.nocolor) {
        path += 'nocolor=true'
    }

    curlGet(`${cfg.country}.getnews.tech`, path)
}

function dictionary(params) {

    if (!params.length) {
        console.warn('What is wrong with you?')

    } else {

        let phrase = '';

        params.forEach((param) => {
            if (phrase.length)
                phrase += ' '

            phrase += param
        })
        
        let curl = spawn('curl', [`dict.org/d:${phrase}`])
        let rawResponse = '';
        
        curl.stdout.on('data', (data) => {
            rawResponse += data;
        })

        curl.on('close', (code) => {
            let lines = rawResponse.split('\r\n')
            let highlight = false
            let output = ''

            lines.forEach((line) => {
                if (
                    !line.startsWith('220') && // Header
                    !line.startsWith('250') && // Status
                    !line.startsWith('150') && // Number of matched defs
                    !line.startsWith('221') && // Don't know yet, says "bye"
                    line !== '.'
                ) {
                    if (line.startsWith('151')) {

                        highlight = true
                    } else {

                        if (highlight) {

                            output += "\r\n"
                            output += chalk.black.bgGreen(` ${line} `)
                            output += "\r\n\r\n"
                            highlight = false

                        } else {

                            output += chalk.cyanBright(line) + "\r\n"
                        }
                    }
                }
            })

            log(output)
        })
    }
}

function curlGet(host, path) {

    const options = {
        hostname: host,
		path: path,
        headers: {
            'User-Agent': 'curl/7.70.0',
        }
    }

    http.get(options, (res) => {
        const { statusCode } = res
        const contentType = res.headers['content-type']
        let error

		if (statusCode !== 200) {
			error = new Error('Request Failed.\n'
				+ `Status Code: ${statusCode}`)
		}

		if (error) {
			console.error(error.message)
			res.resume()
			return
		}

		res.setEncoding('utf-8')
		let rawData = ''

		res.on('data', (chunk) => {rawData += chunk })

		res.on('end', () => {
	  		try {
				console.log(rawData)
	    	} catch (e) {
		 		console.error(e.message)
	  		}
		})
	})
}
